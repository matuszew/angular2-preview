System.register(['angular2/platform/browser', './components/top_navigation.component', './components/hello_world.component', './components/heroes_table.component', 'angular2/http', 'rxjs/Rx'], function(exports_1) {
    var browser_1, top_navigation_component_1, hello_world_component_1, heroes_table_component_1, http_1;
    return {
        setters:[
            function (browser_1_1) {
                browser_1 = browser_1_1;
            },
            function (top_navigation_component_1_1) {
                top_navigation_component_1 = top_navigation_component_1_1;
            },
            function (hello_world_component_1_1) {
                hello_world_component_1 = hello_world_component_1_1;
            },
            function (heroes_table_component_1_1) {
                heroes_table_component_1 = heroes_table_component_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
            },
            function (_1) {}],
        execute: function() {
            browser_1.bootstrap(top_navigation_component_1.TopNavigationComponent);
            browser_1.bootstrap(hello_world_component_1.HelloWorldComponent);
            browser_1.bootstrap(heroes_table_component_1.ActionHeroesTable, [http_1.HTTP_PROVIDERS]);
        }
    }
});
