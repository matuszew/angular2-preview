System.register(['angular2/core', '../services/action_heroes_service.component'], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var __param = (this && this.__param) || function (paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    };
    var core_1, action_heroes_service_component_1;
    var ActionHeroesTable;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (action_heroes_service_component_1_1) {
                action_heroes_service_component_1 = action_heroes_service_component_1_1;
            }],
        execute: function() {
            ActionHeroesTable = (function () {
                function ActionHeroesTable(heroesService) {
                    this.title = 'Angular 2 playground';
                    this.expanded = false;
                    this.drawerMenuItems = ['Hello', 'Bye'];
                    this.heroesService = heroesService;
                }
                ActionHeroesTable.prototype.expandDrawer = function () {
                    this.expanded = true;
                };
                ActionHeroesTable.prototype.discardDrawer = function () {
                    this.expanded = false;
                };
                ActionHeroesTable.prototype.loadTableData = function () {
                    var _this = this;
                    this.heroesService.getHeroesFromUrl().subscribe(function (result) { return _this.heroes = result; });
                };
                ActionHeroesTable.prototype.addHero = function () {
                    var _this = this;
                    this.heroesService.postHero().subscribe(function (result) { return _this.heroes = result; });
                };
                ActionHeroesTable.prototype.getHeroById = function (id) {
                    var _this = this;
                    console.log(id);
                    this.heroesService.getHero({ id: id }).subscribe(function (result) { return _this.heroes = result; });
                };
                ActionHeroesTable = __decorate([
                    core_1.Component({
                        selector: 'action-heroes-table',
                        providers: [action_heroes_service_component_1.ActionHeroesService]
                    }),
                    core_1.View({
                        templateUrl: '/angular_templates/components/table.html'
                    }),
                    __param(0, core_1.Inject(action_heroes_service_component_1.ActionHeroesService)), 
                    __metadata('design:paramtypes', [Object])
                ], ActionHeroesTable);
                return ActionHeroesTable;
            })();
            exports_1("ActionHeroesTable", ActionHeroesTable);
        }
    }
});
