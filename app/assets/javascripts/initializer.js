/**
 * Created by matuszewski on 27/01/16.
 */
System.config({
    packages: {
        app: {
            format: 'register',
            defaultExtension: 'js'
        }
    }
});

System.import('assets/app/services/action_heroes_service.component').then(null, console.error.bind(console));
System.import('assets/app/components/heroes_table.component').then(null, console.error.bind(console));
System.import('assets/app/components/top_navigation.component').then(null, console.error.bind(console));
System.import('assets/app/components/hello_world.component').then(null, console.error.bind(console));
System.import('assets/app/bootstrap').then(null, console.error.bind(console));