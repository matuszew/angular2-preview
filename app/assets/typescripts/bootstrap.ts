import {bootstrap} from 'angular2/platform/browser'
import {TopNavigationComponent} from './components/top_navigation.component'
import {HelloWorldComponent} from './components/hello_world.component'
import {ActionHeroesTable} from './components/heroes_table.component'
import {HTTP_PROVIDERS} from 'angular2/http'
import 'rxjs/Rx'

bootstrap(TopNavigationComponent);
bootstrap(HelloWorldComponent);
bootstrap(ActionHeroesTable, [HTTP_PROVIDERS]);
