import {Component} from 'angular2/core';

@Component({
    selector: 'hello-world',
    template: '<h1>{{welcome}}</h1>'
})
export class HelloWorldComponent {
    welcome = 'My First Angular 2 App'
}
