import {Component, View, Inject} from 'angular2/core';
import { ActionHeroesService } from '../services/action_heroes_service.component';

@Component({
    selector: 'action-heroes-table',
    providers: [ActionHeroesService]
})

@View({
    templateUrl: '/angular_templates/components/table.html'
})

export class ActionHeroesTable {
    title = 'Angular 2 playground';
    drawerMenuItems : Array<String>;
    expanded : boolean ;
    description : string;
    heroes : Array<Object>;
    heroesService : ActionHeroesService

    constructor(@Inject(ActionHeroesService) heroesService ){
        this.expanded = false;
        this.drawerMenuItems = ['Hello', 'Bye'];
        this.heroesService = heroesService;
    }

    expandDrawer(){
        this.expanded = true;
    }

    discardDrawer(){
        this.expanded = false;
    }

    loadTableData(){
        this.heroesService.getHeroesFromUrl().subscribe(result => this.heroes = result);
    }

    addHero(){
        this.heroesService.postHero().subscribe(result => this.heroes = result);
    }

    getHeroById(id){
        console.log(id)
        this.heroesService.getHero({id: id}).subscribe(result => this.heroes = result);
    }
}