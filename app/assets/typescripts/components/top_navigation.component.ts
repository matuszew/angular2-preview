import {Component, View, Inject} from 'angular2/core';

@Component({
    selector: 'top-navigation',
})

@View({
    templateUrl: '/angular_templates/components/navigation/top_navigation.html'
})

export class TopNavigationComponent {
    drawerMenuItems : Array<String>;
    expanded : boolean ;
    title = 'Angular 2 playground';

    constructor(){
        this.expanded = false;
        this.drawerMenuItems = ['Hello', 'Bye']
    }

    expandDrawer(){
        this.expanded = true;
    }

    discardDrawer(){
        this.expanded = false;
    }
}
