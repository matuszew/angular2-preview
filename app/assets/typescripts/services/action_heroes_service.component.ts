import { Http, HTTP_PROVIDERS } from 'angular2/http';
import { Injectable, Component, Inject} from 'angular2/core';

@Component({
    providers: [Http, HTTP_PROVIDERS]
})

@Injectable()
export class ActionHeroesService {
    http: Http

    constructor(@Inject(Http) http : Http){
        this.http = http;
    }

    getHeroesFromUrl(){
        return this.http.get('/action_heroes/get_heroes').map(res => res.json());
    }

    getHero(params){
        return this.http.get('/action_heroes/get_hero_by_id?id='+params.id).map(res => res.json());
    }

    postHero(){
        return this.http.post('/action_heroes/post_hero').map(res => res.json());
    }
}
