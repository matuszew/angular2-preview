class ActionHeroesController < ApplicationController

  HEROES = {
      "batman": {
          id: 1,
        name: 'Batman',
        power: 'Strength & Agility'
      },
      "superman": {
          id: 2,
        name: 'Superman',
        power: 'Strength'
      },
      "america": {
          id: 3,
        name: 'Captain America',
        power: 'Strength & Agility'
      },
      skywalker: {
          id: 4,
          name: 'Luke Skywalker',
          power: 'Jedi Force'
      },
      data: [
           {
              id: 1,
              name: 'Batman',
              power: 'Strength & Agility'
          },
           {
              id: 2,
              name: 'Superman',
              power: 'Strength'
          },
           {
              id: 3,
              name: 'Captain America',
              power: 'Strength & Agility'
          },
           {
              id: 4,
              name: 'Luke Skywalker',
              power: 'Jedi Force'
          }
      ]
}

  def get_heroes
    render json: HEROES[:data]
  end

  def get_hero_by_id
    render json: [HEROES[:skywalker]]
  end

  def post_hero
    render json: HEROES[HEROES[:data].length] = {name: 'New Random Hero', power: 'New Random Power'}
  end


end
